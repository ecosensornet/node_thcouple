Overview
========

.. {# pkglts, glabpkg_dev

.. image:: https://ecosensornet.gitlab.io/node_thcouple/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/node_thcouple/0.0.1/

.. image:: https://ecosensornet.gitlab.io/node_thcouple/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/node_thcouple

.. image:: https://ecosensornet.gitlab.io/node_thcouple/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://ecosensornet.gitlab.io/node_thcouple/

.. image:: https://badge.fury.io/py/node_thcouple.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/node_thcouple



main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/ecosensornet/node_thcouple/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/ecosensornet/node_thcouple/commits/main

.. |main_coverage| image:: https://gitlab.com/ecosensornet/node_thcouple/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/ecosensornet/node_thcouple/commits/main
.. #}

Measure temperature using thermo couples
