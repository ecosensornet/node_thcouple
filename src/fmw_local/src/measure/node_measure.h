#ifndef NODE_MEASURE_H
#define NODE_MEASURE_H

#include "../settings.h"

namespace measure {

typedef struct {
  float tc1;
  float tc1_cal;
  float tc2;
  float tc2_cal;
  float t;
  float t_cal;
  float rh;
  float rh_cal;
} Sample;

void setup();
void setup_fail(String msg);
void sample();

double round_meas(float value);
void fmt_config(String& msg);

}  // namespace measure

extern measure::Sample sample_cur;

#endif //  NODE_MEASURE_H
