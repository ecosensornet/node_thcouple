#include "src/node.h"

void setup() {
#if (DEBUG == 1)
  delay(10000);
  Serial.begin(115200);
  while (!Serial) {
    delay(100);
    Serial.println("waiting for serial");
  }
  delay(300);
  Serial.println("started in :" + String(millis()) + " [ms]");
#endif // DEBUG

  node::setup();
}

void loop() {
  node::loop();
}
