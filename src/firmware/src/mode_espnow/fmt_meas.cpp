#include <ArduinoJson.h>

#include "../config/node_cfg.h"
#include "../measure/node_measure.h"

#include "fmt_meas.h"


void mode_espnow::fmt_meas(String& msg) {
  StaticJsonDocument<256> doc;

  doc["nid"] = node_id;
  doc["iid"] = 3;
  doc["interval"] = cfg.interval;
  doc["nb"] = 1;

  JsonArray events = doc.createNestedArray("events");

  JsonObject events_0 = events.createNestedObject();
  events_0["tc1"] = measure::round_meas(sample_cur.tc1);
  events_0["tc1_cal"] = measure::round_meas(sample_cur.tc1_cal);
  events_0["tc2"] = measure::round_meas(sample_cur.tc2);
  events_0["tc2_cal"] = measure::round_meas(sample_cur.tc2_cal);

  events_0["t"] = measure::round_meas(sample_cur.t);
  events_0["t_cal"] = measure::round_meas(sample_cur.t_cal);
  events_0["rh"] = measure::round_meas(sample_cur.rh);
  events_0["rh_cal"] = measure::round_meas(sample_cur.rh_cal);

  serializeJson(doc, msg);
}

