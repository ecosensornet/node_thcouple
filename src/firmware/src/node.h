#ifndef NODE_BASE_H
#define NODE_BASE_H

#include "settings.h"

namespace node {

void setup();
void loop();

}  // namespace node

#endif //  NODE_BASE_H
