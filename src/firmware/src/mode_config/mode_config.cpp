#include <WiFi.h>
#include <WiFiClient.h>
#include <LittleFS.h>

#include "html_calibration.h"
#include "html_mode_espnow.h"
#include "html_mode_lora.h"
#include "html_mode_wifi.h"
#include "html_sampling.h"

#include "mode_config.h"
#include "html.h"

const char *ssid = "ecosensornet";
const char *password = "floatingworld";

HtmlServer server(80);

void mode_config::setup() {
#if (DEBUG == 1)
  Serial.println("setup config");
#endif
  //   LittleFS.begin(); has been done in node_cfg already

  WiFi.softAP(ssid, password);
#if (DEBUG == 1)
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
#endif

  server.on("/", mode_config::handle_html);
  server.on("/style.css", mode_config::handle_style);
  server.on("/favicon.ico", mode_config::handle_favicon);
  server.on("/calibration.html", calibration::handle_html);
  server.on("/chg_mode_espnow", mode_espnow::handle_chg_mode);
  server.on("/chg_mode_wifi", mode_wifi::handle_chg_mode);
  server.on("/mode_espnow.html", mode_espnow::handle_html);
  server.on("/mode_lora.html", mode_lora::handle_html);
  server.on("/mode_wifi.html", mode_wifi::handle_html);
  server.on("/sampling.html", sampling::handle_html);
  server.on_not_found(mode_config::handle_not_found);

  server.begin();
#if (DEBUG == 1)
  Serial.println("HTTP server started");
#endif
}


void mode_config::loop() {
  server.handleClient();
}

