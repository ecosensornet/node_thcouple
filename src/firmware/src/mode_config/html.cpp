#include "../config/node_cfg.h"
#include "../measure/node_measure.h"
#include "../mode_config/mode_config.h"

#include "html.h"

String meas_cur = "none";

String mode_config::processor(const String& key) {
  if (key == "val_cur") {
    return meas_cur;
  }
  else if (key == "nid") {
    return node_id;
  }

  return "Key not found";
}


void mode_config::handle_html() {
#if (DEBUG == 1)
  Serial.println("serving root");
#endif
  if (server.method() == HTTP_POST) {
#if (DEBUG == 1)
    Serial.println("POST to root.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif

    measure::sample();
    meas_cur = "";
    measure::fmt_config(meas_cur);
  }

  server.process_and_send("/root.html", mode_config::processor);
}

void mode_config::handle_favicon(){
#if (DEBUG == 1)
  Serial.println("serving favicon.ico");
#endif
  server.send_plain("favicon");
}


void mode_config::handle_style() {
#if (DEBUG == 1)
  Serial.println("serving style.css");
#endif
  server.send("/style.css", "text/css");
}


void mode_config::handle_not_found() {
#if (DEBUG == 1)
  Serial.println("serving page not found: " + server.uri());
#endif
  server.send("/not_found.html", "text/html");
}
