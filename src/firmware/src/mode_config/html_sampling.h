#ifndef SAMPLING_HTML_H
#define SAMPLING_HTML_H

#include "../settings.h"

namespace sampling {

String processor(const String& key);
void handle_html();

}  // namespace sampling

#endif //  SAMPLING_HTML_H
