#ifndef CALIBRATION_HTML_H
#define CALIBRATION_HTML_H

#include "../settings.h"

namespace calibration {

String processor(const String& key);
void handle_html();

}  // namespace calibration

#endif //  CALIBRATION_HTML_H
