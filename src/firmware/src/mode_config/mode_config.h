#ifndef MODE_CONFIG_H
#define MODE_CONFIG_H

#include <html_server.h>

#include "../settings.h"

extern HtmlServer server;
extern String meas_cur;

namespace mode_config {

void setup();
void loop();

}  // namespace mode_config

#endif //  MODE_CONFIG_H
