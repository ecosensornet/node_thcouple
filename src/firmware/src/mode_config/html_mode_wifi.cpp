#include "../config/node_cfg.h"
#include "../mode_config/mode_config.h"

#include "html_mode_wifi.h"


String mode_wifi::processor(const String& key) {
 if (key == "lsip0") {
    return String(cfg.lsip[0]);
  }
  else if (key == "lsip1") {
    return String(cfg.lsip[1]);
  }
  else if (key == "lsip2") {
    return String(cfg.lsip[2]);
  }
  else if (key == "lsip3") {
    return String(cfg.lsip[3]);
  }
  else if (key == "interval") {
    return String(cfg.interval);
  }

  return "Key not found";
}


void mode_wifi::handle_html() {
#if (DEBUG == 1)
  Serial.println("serving mode_wifi.html");
#endif
  if (server.method() == HTTP_POST) {
#if (DEBUG == 1)
    Serial.println("POST to mode_wifi.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif

    for (uint8_t i = 0; i < server.args(); i++) {
      if (server.argName(i) == "lsip0") {
        cfg.lsip[0] = server.arg(i).toInt();
      }
      else if (server.argName(i) == "lsip1") {
        cfg.lsip[1] = server.arg(i).toInt();
      }
      else if (server.argName(i) == "lsip2") {
        cfg.lsip[2] = server.arg(i).toInt();
      }
      else if (server.argName(i) == "lsip3") {
        cfg.lsip[3] = server.arg(i).toInt();
      }
      else if (server.argName(i) == "interval") {
        cfg.interval = server.arg(i).toInt();
      }
    }
    save_cfg_file();
  }
  server.process_and_send("/mode_wifi.html", mode_wifi::processor);
}


void mode_wifi::handle_chg_mode() {
#if (DEBUG == 1)
  Serial.println("handle_chg_mode_wifi");
#endif
  cfg_mode = MODE_WIFI;
  ESP.deepSleep(0);
}
