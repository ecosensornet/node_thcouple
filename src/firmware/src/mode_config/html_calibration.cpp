#include "../calibration/calibration.h"
#include "../config/node_cfg.h"
#include "../mode_config/mode_config.h"

#include "html_calibration.h"


String calibration::processor(const String& key) {
  if (key == "val_cur") {
    return meas_cur;
  }
  else if (key == "nid") {
    return node_id;
  }
  else if (key == "tc1_coeff0") {
    return String(calib.tc1[0]);
  }
  else if (key == "tc1_coeff1") {
    return String(calib.tc1[1]);
  }
  else if (key == "tc2_coeff0") {
    return String(calib.tc2[0]);
  }
  else if (key == "tc2_coeff1") {
    return String(calib.tc2[1]);
  }
  else if (key == "t_coeff0") {
    return String(calib.t[0]);
  }
  else if (key == "t_coeff1") {
    return String(calib.t[1]);
  }
  else if (key == "rh_coeff0") {
    return String(calib.rh[0]);
  }
  else if (key == "rh_coeff1") {
    return String(calib.rh[1]);
  }

  return "Key not found";
}


void calibration::handle_html() {
#if (DEBUG == 1)
  Serial.println("serving sampling");
#endif
  if (server.method() == HTTP_POST) {
#if (DEBUG == 1)
    Serial.println("POST to calibration.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif

    for (uint8_t i = 0; i < server.args(); i++) {
      if (server.argName(i) == "tc1_coeff0") {
        calib.tc1[0] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "tc1_coeff1") {
        calib.tc1[1] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "tc2_coeff0") {
        calib.tc2[0] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "tc2_coeff1") {
        calib.tc2[1] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "t_coeff0") {
        calib.t[0] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "t_coeff1") {
        calib.t[1] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "rh_coeff0") {
        calib.rh[0] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "rh_coeff1") {
        calib.rh[1] = server.arg(i).toFloat();
      }
    }
    calibration::save_file();
  }

  server.process_and_send("/calibration.html", calibration::processor);
}

