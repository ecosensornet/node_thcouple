#include "../config/node_cfg.h"
#include "../measure/node_measure.h"
#include "../mode_config/mode_config.h"

#include "html_sampling.h"


String sampling::processor(const String& key) {
  if (key == "val_cur") {
    return meas_cur;
  }
  else if (key == "nid") {
    return node_id;
  }

  return "Key not found";
}


void sampling::handle_html() {
#if (DEBUG == 1)
  Serial.println("serving sampling");
#endif
  if (server.method() == HTTP_POST) {
#if (DEBUG == 1)
    Serial.println("POST to sampling.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif

    measure::sample();
    meas_cur = "";
    measure::fmt_config(meas_cur);
  }

  server.process_and_send("/sampling.html", sampling::processor);
}

