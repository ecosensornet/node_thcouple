#include <ArduinoJson.h>
#include <Wire.h>
#include <sensor_am2322.h>
#include <sensor_max6675.h>

#include "../calibration/calibration.h"

#include "node_measure.h"

#define I2C_SDA 18
#define I2C_SCL 33

TwoWire i2c = TwoWire(0);

measure::Sample sample_cur;

MAX6675 tc1 = MAX6675(9, 11, 12); // thermoCLK, thermoCS, thermoSO
MAX6675 tc2 = MAX6675(9, 7, 12); // thermoCLK, thermoCS, thermoSO
AM2322 hum_temp;

void measure::setup() {
  i2c.setPins(I2C_SDA, I2C_SCL);
  i2c.begin();

  if (!tc1.begin()) {
    setup_fail("tc1 not responding");
  }
  if (!tc2.begin()) {
    setup_fail("tc2 not responding");
  }
  if (!hum_temp.begin(AM2322_I2CADDR, i2c)) {
    setup_fail("hum_temp not responding");
  }
}


void measure::setup_fail(String msg) {
#if (DEBUG == 1)
  Serial.println(msg);
#endif  // DEBUG
  pinMode(LED_BUILTIN, OUTPUT);
  for (uint8_t i = 0; i < 10; i++) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(300);
    digitalWrite(LED_BUILTIN, LOW);
    delay(200);
  }
  ESP.deepSleep(0);
}


void measure::sample(){
    tc1.sample();
    tc2.sample();
	hum_temp.sample();

    while (tc1.sample_not_ready()) {
        delay(10);
    }
    if (tc1.read_sample()) {
        sample_cur.tc1 = tc1.temperature();
        sample_cur.tc1_cal = calib.tc1[0] + calib.tc1[1] * sample_cur.tc1;
    }
    else {
        sample_cur.tc1 = -999;
        sample_cur.tc1_cal = -999;
    }

    while (tc2.sample_not_ready()) {
        delay(10);
    }
    if (tc2.read_sample()) {
        sample_cur.tc2 = tc2.temperature();
        sample_cur.tc2_cal = calib.tc2[0] + calib.tc2[1] * sample_cur.tc2;
    }
    else {
        sample_cur.tc2 = -999;
        sample_cur.tc2_cal = -999;
    }

	while (hum_temp.sample_not_ready()) {
		delay(40);
	}
	if (hum_temp.read_sample()) {
		sample_cur.t = hum_temp.temperature();
		sample_cur.t_cal = calib.t[0] + calib.t[1] * sample_cur.t;
		sample_cur.rh = hum_temp.humidity();
		sample_cur.rh_cal = calib.rh[0] + calib.rh[1] * sample_cur.rh;
	}
	else {
		sample_cur.t = -999;
		sample_cur.t_cal = -999;
		sample_cur.rh = -999;
		sample_cur.rh_cal = -999;
	}
	
}


double measure::round_meas(float value) {
  if (value == -999) {
    return value;
  }
  return (int)(value * 100 + 0.5) / 100.0;
}


void measure::fmt_config(String& msg) {
  StaticJsonDocument<128> doc;

  doc["tc1"] = round_meas(sample_cur.tc1);
  doc["tc1_cal"] = round_meas(sample_cur.tc1_cal);
  doc["tc2"] = round_meas(sample_cur.tc2);
  doc["tc2_cal"] = round_meas(sample_cur.tc2_cal);

  doc["t"] = round_meas(sample_cur.t);
  doc["t_cal"] = round_meas(sample_cur.t_cal);
  doc["rh"] = round_meas(sample_cur.rh);
  doc["rh_cal"] = round_meas(sample_cur.rh_cal);

  serializeJsonPretty(doc, msg);
}
