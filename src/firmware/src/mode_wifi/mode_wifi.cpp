#include <WiFi.h>
#include <HTTPClient.h>
#include <WiFiClient.h>
#include <ArduinoJson.h>

#include "../config/node_cfg.h"
#include "../measure/node_measure.h"

#include "fmt_meas.h"
#include "mode_wifi.h"

const char* gateway_ssid = "raspi-webgui"; // The SSID (name) of the Wi-Fi network
const char* gateway_pwd = "ChangeMe";  // The password of the Wi-Fi network
String url_record;

void mode_wifi::connect_wifi(byte max_try) {
  WiFi.begin(gateway_ssid, gateway_pwd);  // Connect to the network
#if (DEBUG == 1)
  Serial.print("Connecting to ");
  Serial.print(gateway_ssid);
  Serial.println(" ...");
#endif // DEBUG

  // Wait for the Wi-Fi to connect
  for (byte i = 0; (i < max_try) & (WiFi.status() != WL_CONNECTED); i++) {
    delay(1000);
#if (DEBUG == 1)
    Serial.print(i);
    Serial.print(" ");
#endif // DEBUG
  }
#if (DEBUG == 1)
  Serial.println("\n");
#endif // DEBUG
}

void mode_wifi::setup() {
#if (DEBUG == 1)
  Serial.println("setup local wifi");
#endif // DEBUG
  WiFi.mode(WIFI_STA);
  while (WiFi.status() != WL_CONNECTED) {
    connect_wifi(100);
  }
#if (DEBUG == 1)
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());
#endif // DEBUG

  url_record = fmt_url_record();
}


void mode_wifi::loop() {
  unsigned long elapsed = millis();
  WiFiClient client;
  HTTPClient http;
  String msg;

#if (DEBUG == 1)
  Serial.println("loop local wifi");
#endif // DEBUG

  // perform measure
  measure::sample();

  // send message
  if (WiFi.status() != WL_CONNECTED) {
    connect_wifi(10);
  }
  if (WiFi.status() != WL_CONNECTED) {
#if (DEBUG == 1)
    Serial.println("Wifi not connected, msg is lost, will retry next time");
#endif // DEBUG
  }
  else {
#if (DEBUG == 1)
    Serial.println("url: " + url_record);
#endif // DEBUG
    if (http.begin(client, url_record)) {
      http.addHeader("Content-Type", "application/json");
#if (DEBUG == 1)
      Serial.print("[HTTP] POST...\n");
#endif // DEBUG

      mode_wifi::fmt_meas(msg);
#if (DEBUG == 1)
      Serial.print(msg);
#endif // DEBUG
      int httpCode = http.POST(msg);  // httpCode will be negative on error

#if (DEBUG == 1)
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTP] POST... code: %d\n", httpCode);

        // file found at server
        if (httpCode == HTTP_CODE_OK) {
          const String& payload = http.getString();
          Serial.println("received payload:\n<<");
          Serial.println(payload);
          Serial.println(">>");
        }
      } else {
        Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
        Serial.println(">>");
      }
#endif // DEBUG

      http.end();
    }
  }
  elapsed = millis() - elapsed;  // may fail when millis overflow (once every 50 days as been deemed acceptable)
  if (elapsed < cfg.interval * 1e3) {
    delay(cfg.interval * 1e3 - elapsed);
  }

}


String mode_wifi::fmt_url_record() {
  return "http://" + String(cfg.lsip[0]) + "." + String(cfg.lsip[1]) + "." + String(cfg.lsip[2]) + "." + String(cfg.lsip[3]) + ":8002/record";
}
