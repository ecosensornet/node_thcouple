#include <ArduinoJson.h>
#include <LittleFS.h>

#include "calibration.h"

Calibration calib;

void calibration::setup() {
  calibration::load_file();
}


void calibration::load_file() {
  StaticJsonDocument<384> doc;
  File fhr = LittleFS.open("/calib.json", "r");
  deserializeJson(doc, fhr);
  fhr.close();

#if (DEBUG == 1)
  Serial.println("Loaded calib:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif  // DEBUG

  for (uint8_t i = 0; i < 2; i++) {
    calib.tc1[i] = doc["tc1"]["coeffs"][i];
  }
  for (uint8_t i = 0; i < 2; i++) {
    calib.tc2[i] = doc["tc2"]["coeffs"][i];
  }
  for (uint8_t i = 0; i < 2; i++) {
    calib.t[i] = doc["t"]["coeffs"][i];
  }
  for (uint8_t i = 0; i < 2; i++) {
    calib.rh[i] = doc["rh"]["coeffs"][i];
  }

  yield();
}


void calibration::save_file() {
  StaticJsonDocument<384> doc;

  JsonArray tc1_coeffs = doc["tc1"].createNestedArray("coeffs");
  for (uint8_t i = 0; i < 2; i++) {
    tc1_coeffs.add(calib.tc1[i]);
  }

  JsonArray tc2_coeffs = doc["tc2"].createNestedArray("coeffs");
  for (uint8_t i = 0; i < 2; i++) {
    tc2_coeffs.add(calib.tc2[i]);
  }

  JsonArray t_coeffs = doc["t"].createNestedArray("coeffs");
  for (uint8_t i = 0; i < 2; i++) {
    t_coeffs.add(calib.t[i]);
  }

  JsonArray rh_coeffs = doc["rh"].createNestedArray("coeffs");
  for (uint8_t i = 0; i < 2; i++) {
    rh_coeffs.add(calib.rh[i]);
  }

  File fhw = LittleFS.open("/calib.json", "w");
  serializeJsonPretty(doc, fhw);
  fhw.close();

#if (DEBUG == 1)
  Serial.println("Saved calib:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif  // DEBUG

  yield();
}
