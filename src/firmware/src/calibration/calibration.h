#ifndef CALIBRATION_H
#define CALIBRATION_H

#include "../settings.h"

typedef struct {
  float tc1[2];
  float tc2[2];
  float t[2];
  float rh[2];
} Calibration;

extern Calibration calib;

namespace calibration {

void setup();
void load_file();
void save_file();

}  // namespace calibration

#endif //  CALIBRATION_H
