#include <Arduino.h>

#include "calibration/calibration.h"
#include "config/node_cfg.h"
#include "measure/node_measure.h"
#include "mode_config/mode_config.h"
#include "mode_espnow/mode_espnow.h"
#include "mode_lora/mode_lora.h"
#include "mode_wifi/mode_wifi.h"

#include "node.h"

static const uint8_t PIN_MODE_CONFIG = 16;


void node::setup() {
  pinMode(PIN_MODE_CONFIG, INPUT_PULLUP);
#if (DEBUG == 1)
  Serial.println("Setup, mode: " + String(cfg_mode));
#endif // DEBUG
  setup_cfg();
  calibration::setup();

  if (cfg_mode != MODE_CONFIG & digitalRead(PIN_MODE_CONFIG) == LOW) {
#if (DEBUG == 1)
    Serial.println("Force MODE_CONFIG");
#endif // DEBUG
    cfg_mode = MODE_CONFIG;
    ESP.deepSleep(0);
  }

  switch (cfg_mode)  {
    case MODE_CONFIG:
      mode_config::setup();
      break;
    case MODE_ESPNOW:
      mode_espnow::setup();
      break;
    case MODE_LORA:
      mode_lora::setup();
      break;
    case MODE_WIFI:
      mode_wifi::setup();
      break;
    default:
#if (DEBUG == 1)
      Serial.println("userwarning");
      delay(5000);
#endif // DEBUG
      break;
  }

  // measures
  measure::setup();
}


void node::loop() {
  switch (cfg_mode)  {
    case MODE_CONFIG:
      mode_config::loop();
      break;
    case MODE_ESPNOW:
      mode_espnow::loop();
      break;
    case MODE_LORA:
      mode_lora::loop();
      break;
    case MODE_WIFI:
      mode_wifi::loop();
      break;
    default:
#if (DEBUG == 1)
      Serial.println("userwarning");
#endif // DEBUG
      break;
  }
}
